class Board
  attr_accessor :grid

  def self.default_grid
    Array.new(10) {Array.new(10, nil)}
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def count(sym = :s)
    @grid.flatten.compact.count(sym)
  end

  def empty?(pos = nil)
    unless pos.nil?
      @grid[pos[0]][pos[1]].nil?
    else
      count() == 0
    end
  end

  def empty_positions
    empty_arr = []
    (0...@grid.size).each do |y|
      (0...@grid.size).each do |x|
        empty_arr << [y, x] if empty?([y, x])
      end
    end
    empty_arr
  end

  def full?
    @grid.flatten.compact.count == @grid.flatten.count
  end

  def place_random_ship
    raise "Board is full!" if full?()
    pos = empty_positions().sample
    @grid[pos[0]][pos[1]] = :s
  end

  def won?
    count(:s) == 0
  end

  def [](pos)
    @grid[pos[0]][pos[1]]
  end

  def display
    print "  X"; (0...@grid.size).each {|i| print "  " + i.to_s}; puts
    (0...@grid.size).each do |line|
      print "  " + (line).to_s
      @grid[line].each do |el|
        el = "*" if el.nil? || el == :s
        print "  " + el.to_s
      end
      puts
    end
  end
  
end
