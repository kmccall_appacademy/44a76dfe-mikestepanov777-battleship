@grid = [[nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
 [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
  [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
   [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
    [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
     [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
      [nil, nil, :m, nil, :s, nil, nil, nil, nil, nil],
       [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
        [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
         [nil, nil, nil, nil, nil, :x, nil, nil, nil, nil]]

print "  X"; (0...@grid.size).each {|i| print "  " + i.to_s}; puts
(0...@grid.size).each do |line|
  print "  " + (line).to_s
  @grid[line].each do |el|
    el = "*" if el.nil? || el == :s
    print "  " + el.to_s
  end
  puts
end
