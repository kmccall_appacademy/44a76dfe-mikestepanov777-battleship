require_relative "player"
require_relative "board"

class BattleshipGame
  attr_accessor :board, :player, :grid

  def initialize(player = HumanPlayer.new, board = Board.new)
    @player = player
    @board = board
  end

  def attack(pos)
    @board.grid[pos[0]][pos[1]] = :x
    # if @board.grid[pos[0]][pos[1]] == :s
    #   @board.grid[pos[0]][pos[1]] = :x
    #   puts "TARGET DOWN."
    # else
    #   @board.grid[pos[0]][pos[1]] = :m
    #   puts "TARGET ESCAPED!!!"
    # end
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    pos = @player.get_play
    attack(pos)
  end

  def set_game
    puts "Welcome to battleship game!"
    10.times {@board.place_random_ship}
  end

  def play
    set_game
    until game_over?
      @board.display
      play_turn
    end
    "You have destroyed all of the ships!"
  end
end

if __FILE__ == $PROGRAM_NAME
  game = BattleshipGame.new
  game.play
end
